package org.javatraining.sample.samplespring;

/**
 *
 * @author pvik
 */
public interface GreetingService {
    
    public void greet(String msg);
    
}
