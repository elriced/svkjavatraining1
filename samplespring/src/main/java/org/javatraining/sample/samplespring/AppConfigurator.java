package org.javatraining.sample.samplespring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author pvik
 */
@Configuration
public class AppConfigurator {
    
    @Bean("Greeter")
    public GreetingService greetingServiceBean() {
        
        return new GreetingServiceImpl();
    }
    
}
