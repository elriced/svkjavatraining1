package org.javatraining.sample.samplespring;

/**
 *
 * @author pvik
 */
public interface AnotherService {
    public void service(String msg);
}
