package org.javatraining.sample.samplespring.web.controllers;

import java.util.Locale;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author pvik
 */
@Controller
public class HomeController {
    
    private static Logger log = LoggerFactory.getLogger(HomeController.class);
    
    @RequestMapping(value="/", method=RequestMethod.GET)
    public String home(Locale locale, Model m) {
        log.debug("in HomeController.home()");
        
        log.debug("User from: " + locale.getCountry());
        
        return "home";
    }
    
    @RequestMapping(value="/tasks", method=RequestMethod.GET)
    public String tasks(@RequestParam(name="num", required=false) Integer num, Model m) {
        log.debug("in HomeController.tasks()");
        
        m.addAttribute("numTasks", num);
        
        return "tasks";
    }
    
}
