package org.javatraining.sample.samplespring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 *
 * @author pvik
 */
@Component
public class AnotherServiceImplA implements AnotherService {

    private static Logger log = LoggerFactory.getLogger(AnotherServiceImplA.class);
    
    public void service(String msg) {
        log.debug("AnotherServiceImplA: " + msg);
    }
    
}
