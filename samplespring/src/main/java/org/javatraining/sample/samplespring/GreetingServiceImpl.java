package org.javatraining.sample.samplespring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author pvik
 */
@Component("Greeter")
public class GreetingServiceImpl implements GreetingService{

    private static Logger log = LoggerFactory.getLogger(GreetingServiceImpl.class);
    
    @Autowired
    AnotherService anotherService;
    
    public void greet(String msg) {
        
        log.debug(msg);
        
        anotherService.service(msg);
        
    }
    
}
