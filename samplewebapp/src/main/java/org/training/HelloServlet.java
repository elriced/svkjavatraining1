package org.training;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HelloServlet extends HttpServlet {
    
    @Override 
    public void init() throws ServletException{
        super.init();
    }
    
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) 
            throws ServletException, IOException {
        
        PrintWriter pw = resp.getWriter();
        
        String name = req.getParameter("userName");
        
        pw.write("<html>"
                + "<head>"
                + "<title> HelloServlet </title>"
                + "</head>"
                + "<body>"
                + "<h2>");
        if(name != null && ! name.equals("")) {
            pw.write("Hello " + name);    
        } else {
            pw.write("Hello Anonymous");
        }
        
        pw.write("</h2>"
                + "<br>"
                + "Welcome to HelloServlet"
                + "</body>"
                + "</html>");
    }
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) 
            throws ServletException, IOException {

        doGet(req, resp);
        
    }
}