package samplegame.engine.board;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pvik
 */
public class Board {
    
    private List<List<Cell>> board;
    
    public Board() {
        board = new ArrayList<>();
    }
    
    public void initEmptyBoard(int r, int c) {
        
        for (int i=0; i< r; i++) {

            List<Cell> col = new ArrayList<>();
            
            for (int j=0; j<c; j++) {
                col.add(new Cell());
            }
            
            board.add(col);
        }
    
    }
    
}


