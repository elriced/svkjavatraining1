package org.javatraining.sample.samplecms.db.dao.impl;

import java.util.List;
import javax.persistence.EntityTransaction;
import org.javatraining.sample.samplecms.db.DbManager;
import org.javatraining.sample.samplecms.db.dao.PostDao;
import org.javatraining.sample.samplecms.model.Post;

/**
 *
 * @author pvik
 */
public class PostDaoImpl extends PostDao {
    
    @Override
    public void insertPost(Post p) {
        EntityTransaction tx = DbManager.getTransaction();
        
        tx.begin();
        
        DbManager.getEntityManager().persist(p);
        
        tx.commit();
    }

    @Override
    public Post getPost(Long postId) {
        return DbManager.getEntityManager().find(Post.class, postId);
    }

    @Override
    public List<Post> getPostsByUser(Long userId) {
        
        List<Post> posts = DbManager.getEntityManager().createQuery("SELECT p FROM Post p WHERE p.userId = :userid", Post.class)
                .setParameter("userid", userId)
                .getResultList();
       
        return posts;
    }

    @Override
    public void deletePost(Long postId) {
        EntityTransaction tx = DbManager.getTransaction();
        
        tx.begin();
        DbManager.getEntityManager().remove(getPost(postId));
        tx.commit();
    }

    @Override
    public List<Post> getAllPost() {
        List<Post> posts = DbManager.getEntityManager().createQuery("SELECT p FROM Post p", Post.class)
                .getResultList();
       
        return posts;
    }
    
    public int onlyForImpl() {
        return 1;
    }
    
}
