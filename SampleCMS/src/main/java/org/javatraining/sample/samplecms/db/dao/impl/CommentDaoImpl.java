package org.javatraining.sample.samplecms.db.dao.impl;

import java.util.List;
import javax.persistence.EntityTransaction;
import org.javatraining.sample.samplecms.db.DbManager;
import org.javatraining.sample.samplecms.db.dao.CommentDao;
import org.javatraining.sample.samplecms.model.Comment;

/**
 *
 * @author pvik
 */
public class CommentDaoImpl extends CommentDao {

    @Override
    public void insertComment(Comment c) {
        EntityTransaction tx = DbManager.getTransaction();
        
        tx.begin();
        
        DbManager.getEntityManager().persist(c);
        
        tx.commit();
    }

    @Override
    public Comment getComment(Long commentId) {
        return DbManager.getEntityManager().find(Comment.class, commentId);
    }

    @Override
    public List<Comment> getCommentsByPost(Long postId) {
        List<Comment> comments = DbManager.getEntityManager().createQuery("SELECT c FROM Comment c WHERE c.postId = :postid", Comment.class)
                .setParameter("postid", postId)
                .getResultList();
       
        return comments;
    }

    @Override
    public List<Comment> getCommentsByUser(Long userId) {
        List<Comment> comments = DbManager.getEntityManager().createQuery("SELECT c FROM Comment c WHERE c.userId = :userid", Comment.class)
                .setParameter("userid", userId)
                .getResultList();
       
        return comments;
    }

    @Override
    public void deleteComment(Long commentId) {
        EntityTransaction tx = DbManager.getTransaction();
        
        tx.begin();
        DbManager.getEntityManager().remove(getComment(commentId));
        tx.commit();
    }
    
}
