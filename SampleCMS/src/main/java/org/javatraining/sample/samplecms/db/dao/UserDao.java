package org.javatraining.sample.samplecms.db.dao;

import java.util.List;
import org.javatraining.sample.samplecms.model.User;

/**
 *
 * @author pvik
 */
public abstract class UserDao {
    
    public abstract void insertUser(User u);
    
    public abstract User getUser(Long id);
    
    public abstract User getUser(String userid);
    
    public abstract List<User> getAllUsers();
    
}
