package org.javatraining.sample.samplecms.beans;

import java.io.Serializable;

/**
 *
 * @author pvik
 */
public class TestBean implements Serializable{
    
    int prop1;
    
    String prop2;

    public TestBean() {
        
        prop1 =0;
        prop2 = "";
    }
    
    public int sqProp1() {
        return prop1*prop1;
    }
    
    public int getProp1() {
        return prop1;
    }

    public void setProp1(int prop1) {
        this.prop1 = prop1;
    }

    public String getProp2() {
        return prop2;
    }

    public void setProp2(String prop2) {
        this.prop2 = prop2;
    }
    
    
            
    
}
