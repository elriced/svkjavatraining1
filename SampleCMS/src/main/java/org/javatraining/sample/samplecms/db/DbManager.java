package org.javatraining.sample.samplecms.db;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author pvik
 */
public class DbManager {
    
    private static final Logger log = LoggerFactory.getLogger(DbManager.class);
    
    private static final String DB_NAME = "sampleDB";
    
    private static EntityManager entityManager = Persistence.createEntityManagerFactory(DB_NAME).createEntityManager();

    public static void close() {
        entityManager.close();
    }
    
    // Make sure close() is called when DbManager is garbage collected
    @Override
    public void finalize() {
        close();
    }
            
    public static EntityManager getEntityManager() {
        return entityManager;
    }
    
    public static EntityTransaction getTransaction() {
        return entityManager.getTransaction();
    }
}
