package org.javatraining.sample.samplecms.api;

import com.google.inject.Inject;
import java.util.List;
import java.util.logging.Logger;
import javax.persistence.NoResultException;
import org.apache.commons.codec.digest.DigestUtils;
import org.javatraining.sample.samplecms.db.dao.PostDao;
import org.javatraining.sample.samplecms.db.dao.UserDao;
import org.javatraining.sample.samplecms.model.Post;
import org.javatraining.sample.samplecms.model.User;

/**
 *
 * @author pvik
 */
public class CmsApi {

    private final UserDao userDao; // = new UserDaoImpl();
    private final PostDao postDao; // = new PostDaoImpl();
    private static final Logger LOG = Logger.getLogger(CmsApi.class.getName());

    @Inject
    public CmsApi(UserDao u, PostDao p) {
        this.userDao = u;
        this.postDao = p;
    }
    
    private User getUser(String userid) throws CmsApiException {
        User u = null;
        try {
            u = userDao.getUser(userid);
        } catch (NoResultException ex) {
            throw new CmsApiException("User does not exist");
        }
        
        return u;
    }
    
    public static boolean authenticate (User u, String password) 
            throws CmsApiException {
        if (u == null) {
            throw new CmsApiException("User does not exist in the database");
        }
        
        boolean auth = u.getAuthToken().equals(DigestUtils.sha1Hex(password));
        
        if (!auth) {
            throw new CmsApiException("User password does not match");
        }
        
        return auth;        
    }
    
    public boolean authenticate (String userid, String password) 
            throws CmsApiException {
        return authenticate(getUser(userid), password);        
    }
    
    public User authenticateAndGetUser (String userid, String password) 
            throws CmsApiException {
        User u = getUser(userid);
        
        if(authenticate(u, password))
            return u;
        else
            throw new CmsApiException("Authenticateion Error");
        
    }
    
    public static String hash(String plaintext) {
        return DigestUtils.sha1Hex(plaintext);
    }

    public List<Post> getAllPosts() {
        
        return postDao.getAllPost();
    }

}
