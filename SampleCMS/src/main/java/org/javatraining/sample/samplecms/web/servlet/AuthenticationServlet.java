package org.javatraining.sample.samplecms.web.servlet;

import com.google.inject.Inject;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.javatraining.sample.samplecms.App;
import org.javatraining.sample.samplecms.api.CmsApi;
import org.javatraining.sample.samplecms.api.CmsApiException;
import org.javatraining.sample.samplecms.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author pvik
 */
public class AuthenticationServlet extends HttpServlet {
    
    private static Logger log = LoggerFactory.getLogger(AuthenticationServlet.class);
    
    private final CmsApi cmsApi;

    @Inject
    public AuthenticationServlet(CmsApi cmsApi) {
        this.cmsApi = cmsApi;
    }
    
    @Override
    public void init() 
            throws ServletException {
        super.init();
        
        log.debug("init() AuthenticationServlet");
        App.loadTestData();
    }
    
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException {
        log.info("doPost in AuthenticationServlet");
        
        String userName = request.getParameter("username");
        String password = request.getParameter("password");
        
        PrintWriter pw = response.getWriter();
        
        HttpSession session = request.getSession(true);
        
        try {
            User u = cmsApi.authenticateAndGetUser(userName, password);
            
            session.setAttribute("loggedInUser", u);
            
            pw.print("<html><body>"
                    + "Authentication Succesful! <br>"
                    + "<a href=\"index.jsp\">Go Home</a>"
                    + "</body></html>");
            
        } catch (CmsApiException ex) {
            pw.print("Authentication Failed: "+ex.getMessage());
        }
    }
}
