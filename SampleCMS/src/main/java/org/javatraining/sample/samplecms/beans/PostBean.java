package org.javatraining.sample.samplecms.beans;

import com.google.inject.Inject;
import java.io.Serializable;
import java.util.List;
import org.javatraining.sample.samplecms.api.CmsApi;
import org.javatraining.sample.samplecms.model.Post;

/**
 *
 * @author vikramp
 */
public class PostBean implements Serializable{
    
    private final CmsApi cmsApi;

    @Inject
    public PostBean(CmsApi cmsApi) {
        this.cmsApi = cmsApi;
    }
    
    public List<Post> getAllPosts() {
        return cmsApi.getAllPosts();
        
    }
    
    
}


