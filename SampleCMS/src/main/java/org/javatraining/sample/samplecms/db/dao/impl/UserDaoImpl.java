package org.javatraining.sample.samplecms.db.dao.impl;

import java.util.List;
import javax.persistence.EntityTransaction;
import org.javatraining.sample.samplecms.db.DbManager;
import org.javatraining.sample.samplecms.db.dao.UserDao;
import org.javatraining.sample.samplecms.model.User;

/**
 *
 * @author pvik
 */
public class UserDaoImpl extends UserDao {
    
    @Override
    public void insertUser(User u) {
        EntityTransaction tx = DbManager.getTransaction();
        
        tx.begin();
        
        DbManager.getEntityManager().persist(u);
        
        tx.commit();
    }

    @Override
    public User getUser(Long id) {
        return DbManager.getEntityManager().find(User.class, id);
    }

    @Override
    public User getUser(String userid) {
        
        User u = DbManager.getEntityManager().createQuery("SELECT u FROM User u WHERE u.userid = :userid", User.class)
                .setParameter("userid", userid)
                .getSingleResult();
        
        return u;
    }

    @Override
    public List<User> getAllUsers() {
        List<User> users = DbManager.getEntityManager().createQuery("SELECT u FROM User AS u" , User.class)
                .getResultList();
        
        return users;
    }
    
    
    
}
