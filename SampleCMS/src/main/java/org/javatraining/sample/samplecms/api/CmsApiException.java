package org.javatraining.sample.samplecms.api;

/**
 *
 * @author pvik
 */
public class CmsApiException extends Exception {

    CmsApiException(String message) {
        super(message);
    }
    
}
