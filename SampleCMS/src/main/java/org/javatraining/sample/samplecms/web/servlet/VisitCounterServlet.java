package org.javatraining.sample.samplecms.web.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author pvik
 */
public class VisitCounterServlet extends HttpServlet {
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException {
        
        PrintWriter pw = response.getWriter();
        
        HttpSession session = request.getSession(true);
        
        if (session.isNew()) {
            session.setAttribute("visitCounter", new Long(1));
            
            pw.print("Thisis the firsttime you are visiting this servlet!");
        }
        else {
            Long visitCounter = (Long) session.getAttribute("visitCounter");
            
            pw.print("You have visited this site " + visitCounter + " times.");
            
            visitCounter += 1;
            
            session.setAttribute("visitCounter", visitCounter);
        }
        
    }
    
}
