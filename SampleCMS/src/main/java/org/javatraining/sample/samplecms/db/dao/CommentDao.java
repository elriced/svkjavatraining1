package org.javatraining.sample.samplecms.db.dao;

import java.util.List;
import org.javatraining.sample.samplecms.model.Comment;

/**
 *
 * @author pvik
 */
public abstract class CommentDao {
    
    public abstract void insertComment(Comment c);
    
    public abstract Comment getComment(Long commentId);
    
    public abstract List<Comment> getCommentsByPost(Long postId);
    
    public abstract List<Comment> getCommentsByUser(Long userId);
    
    public abstract void deleteComment(Long commentId);
    
}
