/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.javatraining.sample.samplecms.web.servlet;

import com.google.inject.Guice;
import com.google.inject.Injector;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.javatraining.sample.samplecms.CmsModule;

/**
 *
 * @author elric
 */
public class ConfigServlet extends HttpServlet {
    
    @Override
    public void init() {
        Injector i = Guice.createInjector(new CmsModule());
    }
    
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException {
        PrintWriter pw = response.getWriter();
        pw.print("Config");
    }
    
}
