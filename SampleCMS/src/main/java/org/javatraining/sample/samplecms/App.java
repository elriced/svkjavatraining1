package org.javatraining.sample.samplecms;

import org.javatraining.sample.samplecms.api.CmsApi;
import org.javatraining.sample.samplecms.db.dao.PostDao;
import org.javatraining.sample.samplecms.db.dao.UserDao;
import org.javatraining.sample.samplecms.db.dao.impl.PostDaoImpl;
import org.javatraining.sample.samplecms.db.dao.impl.UserDaoImpl;
import org.javatraining.sample.samplecms.model.Post;
import org.javatraining.sample.samplecms.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A Content Management System Backend
 *
 */
public class App 
{
    private static Logger log = LoggerFactory.getLogger(App.class);
        
    public static void loadTestData() {
        
        // Create users
        UserDao udao = new UserDaoImpl();
        
        User u = new User();
        u.setUserid("user1");
        u.setAuthToken(CmsApi.hash("pass1"));
        udao.insertUser(u);
        
        User u2 = new User();
        u2.setUserid("user2");
        u2.setAuthToken(CmsApi.hash("pass2"));
        udao.insertUser(u2);
        
        // Some Posts
        PostDao pdao = new PostDaoImpl();
        
        Post p = new Post();
        p.setTitle("How to Create a Distributed Datastore in 10 Minutes (in Java)");
        p.setLink("http://jodah.net/create-a-distributed-datastore-in-10-minutes");
        p.setUser(u);
        pdao.insertPost(p);
        
        Post p2 = new Post();
        p2.setTitle("How much Java has changed over the years?");
        p2.setContent("How much java has changed from 2002 to now?");
        p2.setUser(u2);
        pdao.insertPost(p2);
        
        Post p3 = new Post();
        p3.setTitle("Design Patterns Through Java");
        p3.setLink("https://www.youtube.com/playlist?list=PLplJltxWVIbJb2KYgFGTdm-TMTq8DKJQw");
        p3.setUser(u);
        pdao.insertPost(p3);
    }
}
