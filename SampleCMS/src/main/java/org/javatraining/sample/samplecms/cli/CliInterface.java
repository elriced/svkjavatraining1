package org.javatraining.sample.samplecms.cli;

import java.util.Scanner;
import org.javatraining.sample.samplecms.model.User;

/**
 *
 * @author pvik
 */
public class CliInterface {
    
    private static final String SHELL_PROMPT = "CMS > ";
    
    private static boolean authenticated = false;
    
    private static User loggedInUser = null;
    
    public static void run() {
        
        System.out.println("Welcome to CMS");
        System.out.println("==============");
        System.out.println("(? for help)");
        
        String cmd = "";
        
        while (! cmd.equalsIgnoreCase("quit")) {
            cmd = getCommand();
            
            if (cmd.equalsIgnoreCase("?") || cmd.startsWith("help")) {
                printHelp(cmd.replace("help", "").replace(" ", ""));
            }
            else if (cmd.equalsIgnoreCase("auth")) {
                if (authenticate()) {
                    System.out.println("Authenticated!");
                } else {
                    System.out.println("Log in failed!");
                }
            }
        }
        
        System.exit(0);
    }
    
    private static void printShell() {
        System.out.print(SHELL_PROMPT);
    }
    
    private static String getCommand() {
        printShell();
        Scanner s = new Scanner(System.in);
        return s.nextLine();
    }

    
    private static void printHelp(String command) {
        
        if (command == null || command.equals("")) {
            System.out.println("CMS Usage help:");
            System.out.println("===============");
            System.out.println("help <command>  -  get more help on a specific command");
            System.out.println("auth            -  Authenticate");
            System.out.println("get             -  Initites the get process");
            System.out.println("post            -  Submit a new Post");
            System.out.println("comment         -  Comment on a post");
            System.out.println("quit            -  Exit");
        }
        else if (command.equalsIgnoreCase("get")) {
            System.out.println("CMS Usage help for command 'get':");
            System.out.println("===============");
            System.out.println("get posts               - retreive all posts");
            System.out.println("get posts <username>    - retreive all posts by <userid>");
            System.out.println("get comments <postid>   - retreive all comments under <postid>");
            System.out.println("get comments <username> - retreive all comments by <username>");
        }
    }

    private static boolean authenticate() {
        String userid;
        String password;
        
        Scanner sc = new Scanner(System.in);
        
        System.out.print("User ID: ");
        userid = sc.nextLine();
        
        System.out.print("Pssword: ");
        password = sc.nextLine();
                
        // Call the api here
        // authenticated = CmsApi.authenticate(userid, password)
        
        authenticated = true; // just a stub till the api method is implemented
        
        //loggedInUser = CmsApi.getUserDetails();
        
        return authenticated;
    }
    
}
