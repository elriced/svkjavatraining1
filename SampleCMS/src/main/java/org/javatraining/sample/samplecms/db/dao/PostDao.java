package org.javatraining.sample.samplecms.db.dao;

import java.util.List;
import org.javatraining.sample.samplecms.model.Post;

/**
 *
 * @author pvik
 */
public abstract class PostDao {
    
    public abstract void insertPost(Post p);
    
    public abstract Post getPost(Long postId);
    
    public abstract List<Post> getPostsByUser(Long userId);
    
    public abstract void deletePost(Long postId);

    public abstract List<Post> getAllPost();
    
}
