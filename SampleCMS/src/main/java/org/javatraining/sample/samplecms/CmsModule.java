package org.javatraining.sample.samplecms;

import com.google.inject.AbstractModule;
import org.javatraining.sample.samplecms.db.dao.PostDao;
import org.javatraining.sample.samplecms.db.dao.UserDao;
import org.javatraining.sample.samplecms.db.dao.impl.PostDaoImpl;
import org.javatraining.sample.samplecms.db.dao.impl.UserDaoImpl;

/**
 *
 * @author pvik
 */
public class CmsModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(UserDao.class).to(UserDaoImpl.class);
        bind(PostDao.class).to(PostDaoImpl.class);
    }
    
}
