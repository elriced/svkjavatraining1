<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>SampleCMS</title>
    </head>
    <body>
        <!-- jsp:include page="header.jsp" / -->
        
        <h1>Sample CMS</h1>
        
        <c:choose>
            <c:when test="${ empty sessionScope.loggedInUser }">
                <a href="login.jsp">Login</a>
            </c:when>
            <c:otherwise>
                Logged in as ${ sessionScope.loggedInUser.userid }.
                <a href="logout.jsp">Logout</a>
            </c:otherwise>
        </c:choose>
        
        <%@include file="footer.html" %>
    </body>
</html>
