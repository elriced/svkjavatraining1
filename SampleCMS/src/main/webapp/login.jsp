<%-- 
    Document   : login
    Created on : Jan 23, 2017, 5:59:33 PM
    Author     : vikramp
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
    </head>
    
    <body>
        
        <c:choose>
            <c:when test="${ empty sessionScope.loggedInUser }">
                <h2>Login</h2>
                <form action="authenticate" method="POST">
                    Username :<input type="text" name="username">
                    Password :<input type="text" name="password">
                    <input type="submit" value="Submit" />
                </form>
            </c:when>
            <c:otherwise>                
                <c:redirect url="index.jsp" />
            </c:otherwise>
            
        </c:choose>
        
        
        <%@include file="footer.html" %>
    </body>
</html>

