<%-- 
    Document   : visitCounter
    Created on : Jan 25, 2017, 10:11:33 PM
    Author     : elric
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Visit Counter</title>
    </head>
    <body>
        <h1>Visit Counter</h1>
        
        <c:choose>
            <c:when test="${ empty sessionScope.visitCounter }">
                You are visiting this page for the first time.
                <c:set scope="session" var="visitCounter" value="1" />
            </c:when>
            <c:otherwise>
                You are visiting this page ${sessionScope.visitCounter} time.
                <c:set scope="session" var="visitCounter" value="${sessionScope.visitCounter+1}" />
            </c:otherwise>
        </c:choose>
        
    </body>
</html>
