<%-- 
    Document   : header
    Created on : Jan 26, 2017, 10:49:06 PM
    Author     : elric
--%>
<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<p align=right">
<c:choose>
    <c:when test="${ empty sessionScope.loggedInUser }">
        <a href="login.jsp">Login</a>
    </c:when>
    <c:otherwise>                
        <a href="logout.jsp">Logout</a>
    </c:otherwise>
</c:choose>
</p>
    