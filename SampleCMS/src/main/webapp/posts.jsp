<%-- 
    Document   : posts.jsp
    Created on : Jan 24, 2017, 11:06:15 PM
    Author     : pvik
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>SampleCMS: Posts</title>
    </head>
    
    <jsp:useBean id="postBean"
                 scope="page"
                 class="org.javatraining.sample.samplecms.beans.PostBean" />
    <body>
        <!-- jsp:include page="header.jsp" /-->
        <h1>Posts</h1>
        
        <table>
            <c:forEach var="post" items="${ postBean.getAllPosts() }" >
                <tr>
                    <td>${ post.id }</td>
                    <td>${ post.title}</td>
                    <td>${ post.content }</td>
                    <td>Posted By ${ post.user.userid }</td>
                </tr>
            </c:forEach>
        </table>
        
        <a href="addPost.jsp">Add new Post</a>
        
        <%@include file="footer.html" %>
    </body>
</html>
